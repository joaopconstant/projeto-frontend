# **Projeto Front-End**



# Repositório de Recursos

## - HTML
https://developer.mozilla.org/pt-BR/docs/Web/HTML
    
https://www.w3schools.com/html/

### Exemplo de código:
```
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>

<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

</body>
</html> 
```

## - CSS
https://developer.mozilla.org/pt-BR/docs/Web/CSS

https://www.w3schools.com/css/

### Exemplo de código:
```
body {
  background-color: lightblue;
}

h1 {
  color: white;
  text-align: center;
}

p {
  font-family: verdana;
  font-size: 20px;
}
```

## - JavaScript
https://developer.mozilla.org/pt-BR/docs/Web/JavaScript

https://www.w3schools.com/js/

### Exemplo de código
```
<!DOCTYPE html>
<html>
<body>

<h2>What Can JavaScript Do?</h2>

<p id="demo">JavaScript can change HTML content.</p>

<button type="button" onclick='document.getElementById("demo").innerHTML = "Hello JavaScript!"'>Click Me!</button>

</body>
</html>
```

## - React
https://pt-br.reactjs.org/docs/getting-started.html

https://www.w3schools.com/react/

### Exemplo de código:
```
import React from 'react';
import ReactDOM from 'react-dom/client';

function Hello(props) {
  return <h1>Hello World!</h1>;
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Hello />);

```

## - Node
https://github.com/nodejs/node

https://www.w3schools.com/nodejs/default.asp

# Exemplo de código:
```
var http = require('http');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World!');
}).listen(8080); 
```
